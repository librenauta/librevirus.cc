---
layout: post
title: Configurar servidor web
date: 2020-03-22 20:02:23
author: librevirus.cc
short_description: instalar un servidor web en nuestra computadora o raspberry pi
---
#### Configurar servidor web

Para ver un procedimiento paso a paso desde 0 ir a [labekka.red](https://0xacab.org/labekka/fanzine-servidora-web-feminista) o descargar los [pdf](httsp://librevirus.cc/pdf/la_bekka.zip)

1. Instalar nginx

>`sudo apt-get install nginx` para instalarlo

>`sudo systemctl start nginx` para iniciarlo

>`sudo systemctl status nginx` para ver su estado

Para editar la configuración del archivo de nginx, que es el que permite realizar las conexiones buscaremos el sigueinte archivo

> `sudo nano /etc/nginx/nginx.conf`

>`cd /etc/nginx` parados en /nginx, hacemos una copia de seguridad de la configuracion estandar

> `cp nginx.conf nginx~.conf`

y editamos con nano(editor de txt), vi o gedit

> `$ sudo nano nginx.conf`

agregamos el siguiente server {}
```
server {
    listen [::]:80;
    server_name [poner aqui ip sin corchetes];

    location / {
        proxy_pass http://127.0.0.1:8080;
    }}
```
