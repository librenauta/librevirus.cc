---
layout: post
title: Configurar calibre y ejecutar calibre-server
date: 2020-03-21 20:05:00
author: librevirus.cc
short_description: paso a paso de como instalar calibre, y montar una biblioteca para luego configurarla como servidora
---
## 1. Bases generales

![Logotipo de Calibre](https://calibre-ebook.com/resources/img/logo.png)

Cada una de estas bibliotecas está hecha con [Calibre](https://calibre-ebook.com/), un gestor de libros y archivos digitales.

Además de convertir a diferentes formatos, Calibre permite establecer un servidor local para poder acceder de forma remota a nuestra biblioteca.

Lo que haremos en las siguientes líneas es configurar una computadora —de escritorio o una Raspberry Pi— con esta función.

![Logotipo de Nginx](https://nginx.org/nginx.png)

Para que nuestra biblioteca sea de fácil acceso, con el servidor web Nginx configuraremos una URL específica para su acceso.

## 2. Instalación del *software* necesario

### A. En Debian Linux y su familia

La familia Debian comprende:

* Debian
* Raspbian —sistema operativo para Raspberry Pi—
* Ubuntu
* Linux Mint

Desde la terminal, ejecuta:

```
apt install calibre nginx
```

### B. En Arch Linux

Desde la terminal, ejecuta:

```
pacman -S calibre nginx
```

### C. En MacOS

Antes de instalar debes contar con [Homebrew](https://brew.sh). Después, desde la terminal ejecuta:

```
brew install calibre nginx
```

### D. En Windows

¿Sabes cómo instalar Calibre y Nginx? [Escríbenos](mailto:hola@librevirus.cc) para poder sumar el tutorial.

## 3. Configuración de Calibre

A continuación **vamos a suponer que te encuentras ubicada en tu [carpeta de usuaria](https://es.wikipedia.org/wiki/Carpeta_de_usuario)**. Si no estás segura de estar ahí, entonces desde la terminal ejecuta lo siguiente para ir a la raíz de tu directorio:

```
cd
```

### A. Generación de una nueva biblioteca

Si aún no cuentas con una biblioteca de Calibre, puedes generar una nueva al ejecutar:

```
calibredb list
```

Esto te generará una biblioteca vacía, en la sección 3.F. te explicamos cómo puedes agregar libros.

### B. Duplicación de una biblioteca existente

Si ya cuentas con una biblioteca de Calibre, copia su directorio junto con el archivo `metadata.db`.

Si ya tenemos una biblioteca y queremos generar una nueva para compartir determinado contenido, podemos ir a: cambiar o crear biblioteca > luego ir a nuestra biblioteca inicial y seleccionar los libros > click derecho > copiar a la biblioteca > selecionar la biblioteca recién creada.

Si estás conectada a la Rasperry Pi desde otra computadora, puedes copiar los archivos usando OpenSSH:

```
scp -r /mi/directorio/biblioteca pi@miIP:
```

> **ATENCIÓN**. Sustituye `pi` por la usuaria y `miIP` por la IP de tu computadora.

Esta operación la puedes utilizar para copiar cualquier tipo de archivo y en cualquier momento a tu Raspberry Pi.

### C. Ejecución de la biblioteca

Existen varias maneras de poner a andar tu biblioteca con `calibre-server` según la configuración deseada.





------
## Configurar calibre y ejecutar calibre-server

Instalar calibre:
> `sudo apt-get install calibre` en debian/raspbian

> si tenemos monitor conectado a nuestra computadora/Rpi, podemos crear una biblioteca en calibre e incluir los libros que queremos compartir. esto generará un directorio que contrendrá los directorios con los libros y un archivo llamado metadata.db **este directorio es importante**

> si ya tenemos una biblioteca y queremos generar una nueva para compartir determinado contenido, podemos ir a: cambiar o crear biblioteca > luego ir a nuestra biblioteca inicial y seleccionar los libros > click derecho > copiar a la biblioteca > selecionar la biblioteca recién creada.

> si no tenemos monitor para ver nuestra computadora/Rpi, podemos crear nuestra biblioteca como en el paso anterior en nuestra computadora y luego copiar el directorio de la ubicación a nuestra computadora remota

El siguiente comando copia un directorio junto con sus archivos en /home/pi/biblioteca
> `scp -r /mi/directorio/biblioteca pi@miIP:/home/pi/biblioteca`

Si ejecutamos calibre-server sin autentificación , desde afuera vemos los libros como sólo lectura, así nadie puede borrar nuestra biblioteca.

entonces ejecutamos la siguiente sentencia para correr calibre-server en el puerto 8080:
>`sudo calibre-server --ban-after=10 --timeout=600 /home/pi/nombrecarpetabiblioteca`

si ya tenés un servicio ejecutadose en el puerto 8080, agregando `--port 8085` podés modificar el puerto a usar por calibre-server
>`sudo calibre-server --ban-after=10 --timeout=600 /home/pi/nombrecarpetabiblioteca --port 8085`

## Ejecutar calibre-server como servicio

Luego si estamos en otra pc y queremos configurar un servicio para que se este ejecutando calibre server cuando cortemos la conexión, podemos seguir los siguientes pasos.
primero vamos a crear un archivo en `/etc/systemd/system/calibre-server.service`:

`sudo nano /etc/systemd/system/calibre-server.service`
y pegamos lo siguiente
```
[Unit]
Description=calibre content server
After=network.target

[Service]
Type=simple
User=pi
Group=pi
ExecStart=/usr/bin/calibre-server --ban-after=10 --timeout=600 "/home/pi/mibiblioteca/"

[Install]
WantedBy=multi-user.target
```
1. `/usr/bin/calibre-server` es la ubicación  donde se ejecuta calibre-server,  depende como lo instalen aveces esta en otro lado, para buscar esa ruta puede escribir `find | grep calibre-server`
2. `"home/pi/mibliblioteca"` es el directorio que contiene la biblioteca

apretamos `ctrl+x` + `y` para guardar

Luego para iniciar el servicio:

`sudo systemctl start calibre-server`

`sudo systemctl status calibre-server` para ver el estado (si levantó bien)

`sudo systemctl stop calibre-server` para frenarlo

hasta acá lo que se hace con calibre, nuestro servidor de la biblioteca virtual, ahora lo siguiente es configurar la [servidora web]({{ site.url }}/2020/03/22/Configurar-servidora-web.html)

* este paso a paso fue creado con el soporte de [@perro_tuerto](https://mastodon.social/@_perroTuerto) <3 y [@librenauta](https://mastodon.social/@librenauta)

fuentes:
1. [calibre-server](https://manual.calibre-ebook.com/server.html#creating-a-service-for-the-calibre-server-on-a-modern-linux-system)
2. [mobileread](https://www.mobileread.com/forums/showthread.php?t=288408)

[Service]
