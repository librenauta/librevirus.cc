---
layout: post
title: Agregar mi biblioteca
short_description: contactate y agregamos tu biblioteca
---
# Como sumo una biblioteca?
**Librevirus** está disponible para todas aquellas personas que quieren compartir la parte de su casa digital donde guardan la biblioteca.

Para ello, dinos un subdominio `.librevirus.cc` y la IP desde donde nos abres las puertas a tus libros, y lo documentaremos en esta misma página.

### Pasos
Escríbenos [aquí](mailto:hola@librevirus.cc) diciéndonos:
1. Cosas bonitas.
2. Qué subdominio `.librevirus.cc` quieres tener para tu biblioteca.
3. A qué IP tenemos que dirigir el subdominio y si fuese necesario su puerto.
4. Una breve descripción de qué se encontrará en ella.
5. Adjunta una foto (1280px X 960px) de tu RaspberryPi o computadora para que se vea donde vive tu Librevirus.

Desde Librevirus.cc creemos que compartir libremente contenidos es la base de toda estructura internauta sana. Lamentablemente, desde muchos gobiernos se toman medidas basadas en una hipócrita interpretación del acceso justo a aquellos. Por ello no podemos más que recomendarte que subas contenidos con licencias libres como [ppl](https://endefensadelsl.org/ppl_deed_es.html), [leal](https://gitlab.com/programando-libreros/juridico/licencia-editorial-abierta-y-libre) o [cc](https://creativecommons.org/) o dominio público, para así no meterte en algún lío de esos que dan mucho dolores de cabeza. De todos modos, el servidor es tuyo —> la responsabilidad es tuya. Cuando metemos contenidos en servidores de empresas ellas se lucran de tus datos justamente porque se lo permites. Aquí tu eliges.
Vamos a jugar, siempre."
