---
layout: default
permalink: docs
---

# Docs

## Crear y compartir tu biblioteca virtual

<section>
  <h2></h2><br>

  {% for post in site.posts %}
    {% assign nth = forloop.index0 | modulo:2 %}
    <div class="media">


      <div class="media-body">
        <div class="media-heading">
          <span> &raquo;
            {% if post.external_url %}
              <a href="{{ post.external_url }}" target="_blank" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% else %}
              <a href="{{ post.url | prepend: site.baseurl }}" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% endif %}
          </span>
        </div>
        <div class="media-content">
          {{ post.short_description }}
        </div>
      </div>

    </div>
  {% endfor %}
</section>
  <h2></h2><br>
<section>
  <h2>Lista de bibliotecas confinadas</h2>
  <p>
     <a href="{{ "/bibliotecas.html" | prepend: site.baseurl }}">Aquí</a> para explorar las que existen :)
  </p>
</section>
